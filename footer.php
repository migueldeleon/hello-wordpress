<footer id="footer">
	<div class="wrap">
		<ul class="footer_divs">
			<li><?php dynamic_sidebar( 'footer_1' ); ?></li>
			<li><?php dynamic_sidebar( 'footer_2' ); ?></li>
			<li><?php dynamic_sidebar( 'footer_3' ); ?></li>
		</ul>
	</div>
	<div class="copy">
		<p>Potenciado por Wordpress. <strong>Hello Theme</strong> fue diseñado por <a href="http://migueldeleon.co">Migueldeleon.co</a></p>
	</div>
</footer>
</body>
</html>