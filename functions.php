<?php
//=========================
//Eliminamos la barra del administrador
//=========================
add_action('get_header', 'remove_admin_login_header');
function remove_admin_login_header() {
    remove_action('wp_head', '_admin_bar_bump_cb');
}


//=========================
//Registrando el menú
//=========================
function menu_principal() {
	register_nav_menu('menu', __('Menú principal'));
}
add_action('init', 'menu_principal');

//=========================
//Sidebar
//=========================
if ( function_exists('register_sidebar') ){
    register_sidebar(array(
    	'name' => 'Sidebar',
    	'id' => 'Sidebar_principal',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>',
    ));
    register_sidebar(array(
    	'name' => 'Footer 1',
    	'id' => 'footer_1',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>',
    ));
    register_sidebar(array(
    	'name' => 'Footer 2',
    	'id' => 'footer_2',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>',
    ));
    register_sidebar(array(
    	'name' => 'Footer 3',
    	'id' => 'footer_3',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>',
    ));
}


//=========================
//Leer más
//=========================
add_filter( 'the_content_more_link', 'modify_read_more_link' );
function modify_read_more_link() {
return '<a class="more-link" href="' . get_permalink() . '">Seguir leyendo...</a>';
}

?>