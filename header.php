<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' |'; } ?> <?php bloginfo('name'); ?></title>
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/modernizr.js"></script>
        <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" type="text/css" media="screen" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
        <?php wp_head(); ?>
    </head>
<body>
<header id="header">
	<div class="wrap">
		<div class="logo"><h1>Hello</h1></div>
		<div class="menu">
			<?php wp_nav_menu( array( 'theme_location' => 'menu') ); ?>
		</div>
	</div>
</header>
<div class="destacados">
<?php
query_posts('cat="destacados"&posts_per_page=4&offset=1');
if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div class="post" id="post-<?php the_ID(); ?>">
		<h2><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
	</div>
<?php endwhile; ?>
<?php else : ?>
	
<?php endif; ?>
<?php wp_reset_query(); ?>
</div>