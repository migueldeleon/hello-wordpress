<?php get_header(); ?>

<div id="contenido">
	<div class="loop">	
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div <?php post_class(); ?>>
			<h2 class="titulopost"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<ul class="datos_post" style="display:none;">
				<li class="fecha">Publicado el <?php the_time('j \d\e\ F \d\e\ Y'); ?></li>
				<li class="porautor">Por <?php the_author(); ?></li>
			</ul>
			<p><?php the_content(); ?></p>
			<div class="bl"></div>
		</div>
	<?php endwhile; else: ?>
	<p><?php _e('Aún no hay post en este blog.'); ?></p>
	<?php endif; ?>
	</div>
	<?php get_sidebar(); ?>
	<div class="fix"></div>
</div>

<?php get_footer(); ?>